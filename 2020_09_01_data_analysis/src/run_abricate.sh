#!/bin/bash

mkdir ../data/atbr_predictions

##-----------------3. RUN ABRICATE TO GET THE ATB-R genes in PLASMIDS -------------------------##
cd ../data/atbr_predictions

abricate --fofn ../plasmids_mash_distances/all_plasmids_list.txt --csv > all_plasmids_atbr.csv 
