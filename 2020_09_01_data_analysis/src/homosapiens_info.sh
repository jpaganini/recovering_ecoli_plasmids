#!/bin/bash

#get the list of bioprojects
list=$(cat ../../2020_08_30_run_predictions/results/homosapiens_bioprojects.csv | sed 's/"//g')

mkdir ../data/bioprojects_data

for bioprojects in $list
do
esearch -db bioproject -query ${bioprojects} | efetch -format docsum > ../data/bioprojects_data/${bioprojects}.txt
done

#go to the folder
cd ../data/bioprojects_data
for bioprojects in $list;
do 
description=$(grep '<Project_Description>' ${bioprojects}.txt | sed 's/<Project_Description>//g' | sed 's/<[/]Project_Description>//g' ) 
title=$(grep '<Project_Title>' ${bioprojects}.txt | sed 's/<Project_Title>//g' | sed 's/<[/]Project_Title>//g' ) 
echo -e ${bioprojects}'\t'${title}','${description} >> bioproject_summary.tsv
done 
