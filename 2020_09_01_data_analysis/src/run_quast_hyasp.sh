#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

while getopts c: flag
do
  	case "${flag}" in
                c) conda=${OPTARG};;
        esac
done

source ${conda}/etc/profile.d/conda.sh

##----------------------------

conda activate quast_mmbioit

#mkdir for the general results

#mkdir ../results/quast_output/hyasp
#mkdir ../results/quast_output/gplas
#mkdir ../results/quast_output/plasmid_spades
#mkdir ../results/quast_output/fishing_for_plasmids
#mkdir ../results/quast_output/scapp

##----------run quast for HyAsP predictions --------------------#

#move to the folder that holds the hyasp_predictions - THIS WILL HAVE TO BE REPLACED BY cd ../hyasp/predictions/
cd ../../tools/hyasp/predictions

#this can be removed from the final script
files=$(ls)

#run quast, the target is aligning each individual prediction to the complete reference genome to then extract the data
for strains in $files
do
cd ${strains}
all_bins=$(ls ${strains}*.fasta | sed 's/.fasta//g')
for bin in $all_bins
do
quast -o ../../../../2020_09_01_data_analysis/results/quast_output/hyasp/${strains}/${bin} -r ../../../../2020_08_25_ecoli_metadata/data/fasta_final/${strains}_genomic.fna -m 1000 -t 8 -i 500 --no-snps --ambiguity-usage all ${bin}.fasta
done
cd ../
done
