#! /bin/bash

#get mob predictions
cd ../../tools/mob_predictions/
mob_files=$(ls)
for strains in $mob_files
do
mob_number=$(ls -1 ${strains}/plasmid*fasta | wc -l)
echo ${strains},${mob_number} >> ../../2020_09_01_data_analysis/data/mob_plasmids_count.csv
done

#get hyasp predictions
cd ../hyasp/predictions
hyasp_files=$(ls)
for strains in $hyasp_files
do
hyasp_number=$(ls -1 ${strains}/${strains}_bin_*fasta | wc -l)
echo ${strains},${hyasp_number} >> ../../../2020_09_01_data_analysis/data/hyasp_plasmids_count.csv
done

#get spades predictions
cd ../../spades_predictions/
spades_files=$(ls)
for strains in $spades_files
do
spades_number=$(ls -1 ${strains}/${strains}_component_*fasta | wc -l)
echo ${strains},${spades_number} >> ../../2020_09_01_data_analysis/data/spades_plasmids_count.csv
done

#get scapp predictions
cd ../scapp_predictions
scapp_files=$(ls)
for strains in $scapp_files
do
scapp_number=$(ls -1 ${strains}/${strains}_RNODE_*fasta | wc -l)
echo ${strains},${scapp_number} >> ../../2020_09_01_data_analysis/data/scapp_plasmids_count.csv
done

#get fishing predicions
cd ../FishingForPlasmids/data/FFP_output/
fishing_files=$(ls)
for strains in $fishing_files
do
fishing_number=$(ls -1 ${strains}/${strains}*fasta | wc -l)
echo ${strains},${fishing_number} >> ../../../../2020_09_01_data_analysis/data/fishing_plasmids_count.csv
done

#get gplas results
cd ../../../gplas/results/
#I will use the fishing files
for strains in $fishing_files
do
gplas_number=$(ls -1 ${strains}_bin_* | grep -v Unbinned | wc -l)
echo ${strains},${gplas_number} >> ../../../2020_09_01_data_analysis/data/gplas_plasmids_count.csv
done
