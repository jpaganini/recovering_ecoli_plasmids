#!/bin/bash

#get a list of strains that will be used for benchmarking
files=$(cat ../../2020_08_30_run_predictions/results/benchmark_list.csv | sed 's/"//g')


##-----------------1. RUN MASH FOR  THE PLASMIDS PRESENT IN OUR BENCHMARKING DATA-SET -------------------------##

#1. Run mash to get the genomic distances between the plasmids

#mkdir for mash results
mkdir ../data/plasmids_mash_distances
cd ../data/plasmids_mash_distances

#Create a txt file that contains a list of paths to the files.
for strains in $files
do 
if [ -f "../../../2020_08_30_run_predictions/data/separated_genomes/${strains}/${strains}_plasmids.fna" ]
then
echo ../../../2020_08_30_run_predictions/data/separated_genomes/${strains}/${strains}_plasmids.fna >> file_list.txt
else
echo ${strains} >> ../benchmark_strains_no_plasmids.txt
fi
done

#Create a mash sketch
mash sketch -i -l file_list.txt -o plasmid_sketches -k 21 -s 1000

#get the mash distances between the plasmids
mash dist -i -t plasmid_sketches.msh -l file_list.txt > plasmid_distances.tab

##--------------------2. RUN MASH FOR ALL THE PLASMIDS PRESENT  --------------------------------------------###

all_plasmids=$(cat ../../../2020_08_25_ecoli_metadata/results/final_strain_list.csv | sed 's/"//g' | sed 's/_genomic//g' )

#Create a txt file that contains a list of paths to the files.
for strains in $all_plasmids
do 
if [ -f "../../../2020_08_30_run_predictions/data/separated_genomes/${strains}/${strains}_plasmids.fna" ]
then
echo ../../../2020_08_30_run_predictions/data/separated_genomes/${strains}/${strains}_plasmids.fna >> all_plasmids_list.txt
else
echo ${strains} >> ../all_strains_no_plasmids.txt
fi
done

#Create a mash sketch
mash sketch -i -l all_plasmids_list.txt -o all_plasmid_sketches -k 21 -s 1000

#get the mash distances between the plasmids
mash dist -i -t all_plasmid_sketches.msh -l all_plasmids_list.txt > all_plasmid_distances.tab

 
