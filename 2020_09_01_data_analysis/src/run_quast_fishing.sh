#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

while getopts c: flag
do
  	case "${flag}" in
                c) conda=${OPTARG};;
        esac
done

source ${conda}/etc/profile.d/conda.sh

##----------------------------

conda activate quast_mmbioit

###------------run quast for fishing_for_plasmids---------------------------------####

#move to the folder that holds the mob_predictions
cd ../../tools/FishingForPlasmids/data/FFP_output

#get a list of all the strains included in the benchmakr, there is one folder for each file
files=$(ls)

#run quast, the target is aligning each individual prediction to the complete reference genome to then extract the data
for strains in $files
do
cd ${strains}
all_bins=$(ls *fasta | sed 's/.fasta//g')
for bin in $all_bins
do
quast -o ../../../../../2020_09_01_data_analysis/results/quast_output/fishing_for_plasmids/${strains}/${bin} -r ../../../../../2020_08_25_ecoli_metadata/data/fasta_final/${strains}_genomic.fna -m 1000 -t 8 -i 500 --no-snps --ambiguity-usage all ${bin}.fasta
done
cd ../
done

#-------had to run four bins individually because an issue with the name of the bin
cd GCA_014170595.1_ASM1417059v1
#change the name of the bin to remove the '*' that is giving problems
mv GCA_014170595.1_ASM1417059v1_inci1-ST3*.fasta GCA_014170595.1_ASM1417059v1_inci1-ST3.fasta 

#run quast
quast -o ../../../../../2020_09_01_data_analysis/results/quast_output/fishing_for_plasmids/GCA_014170595.1_ASM1417059v1/GCA_014170595.1_ASM1417059v1_inci1-ST3 -r ../../../../../2020_08_25_ecoli_metadata/data/fasta_final/GCA_014170595.1_ASM1417059v1_genomic.fna -m 1000 -t 8 -i 500 --no-snps --ambiguity-usage all GCA_014170595.1_ASM1417059v1_inci1-ST3.fasta

#--------------------------------------------------------------------------------------

cd ../GCA_013402855.1_ASM1340285v1
#change the name of the bin to remove the '*' that is giving problems
mv GCA_013402855.1_ASM1340285v1_inci1-ST3*.fasta GCA_013402855.1_ASM1340285v1_inci1-ST3.fasta

#run quast
quast -o ../../../../../2020_09_01_data_analysis/results/quast_output/fishing_for_plasmids/GCA_013402855.1_ASM1340285v1/GCA_013402855.1_ASM1340285v1_inci1-ST3 -r ../../../../../2020_08_25_ecoli_metadata/data/fasta_final/GCA_013402855.1_ASM1340285v1_genomic.fna -m 1000 -t 8 -i 500 --no-snps --ambiguity-usage all GCA_013402855.1_ASM1340285v1_inci1-ST3.fasta

#-------------------------------------------------------------------------------------------

cd ../GCA_013830785.1_ASM1383078v1
#change the name of the bin to remove the '*' that is giving problems
mv GCA_013830785.1_ASM1383078v1_inchi2-ST11*.fasta GCA_013830785.1_ASM1383078v1_inchi2-ST11.fasta

#run quast
quast -o ../../../../../2020_09_01_data_analysis/results/quast_output/fishing_for_plasmids/GCA_013830785.1_ASM1383078v1/GCA_013830785.1_ASM1383078v1_inchi2-ST11 -r ../../../../../2020_08_25_ecoli_metadata/data/fasta_final/GCA_013830785.1_ASM1383078v1_genomic.fna -m 1000 -t 8 -i 500 --no-snps --ambiguity-usage all GCA_013830785.1_ASM1383078v1_inchi2-ST11.fasta

#-------------------------------------------------------------

cd ../GCA_013900205.1_ASM1390020v1
#change the name of the bin to remove the '*' that is giving problems
mv GCA_013900205.1_ASM1390020v1_inci1-ST267*.fasta GCA_013900205.1_ASM1390020v1_inci1-ST267.fasta

#run quast
quast -o ../../../../../2020_09_01_data_analysis/results/quast_output/fishing_for_plasmids/GCA_013900205.1_ASM1390020v1/GCA_013900205.1_ASM1390020v1_inci1-ST267 -r ../../../../../2020_08_25_ecoli_metadata/data/fasta_final/GCA_013900205.1_ASM1390020v1_genomic.fna -m 1000 -t 8 -i 500 --no-snps --ambiguity-usage all GCA_013900205.1_ASM1390020v1_inci1-ST267.fasta
