#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

while getopts c: flag
do
  	case "${flag}" in
                c) conda=${OPTARG};;
        esac
done

source ${conda}/etc/profile.d/conda.sh

#-----------------------------------------------------------------------#

#activate environment
conda activate Plascad  #replace with plascad_mmbioit

#1. create directory for holding input data for plascad
mkdir ../data/plascad_input_fasta

#2. Gather a list of strains
strain_list=$(cat ../results/final_strain_benchmark.csv)

#3. Cat all the proper fasta files into one file
for strain in $strain_list
do
cat ../../2020_08_30_run_predictions/data/separated_genomes/${strain}/${strain}_plasmids.fna >> ../data/plascad_input_fasta/all_plasmid_benchmakr_sequences.fasta
done

#4. Run Plascad
Plascad -i ../data/plascad_input_fasta/all_plasmid_benchmakr_sequences.fasta

#5. Move output to the results folder
mkdir ../results/plascad_output
mv ../data/plascad_input_fasta/*txt ../results/plascad_output
