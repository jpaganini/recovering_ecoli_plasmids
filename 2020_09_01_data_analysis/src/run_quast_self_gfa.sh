#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

while getopts c: flag
do
  	case "${flag}" in
                c) conda=${OPTARG};;
        esac
done

source ${conda}/etc/profile.d/conda.sh

##----------------------------

conda activate quast_mmbioit

#mkdir for the general results

mkdir ../results/quast_output/self_alignments_gfa

##----------run quast for self-alignment --------------------#

#move to the folder that holds the contigs obtained from the gfa files from every strain (we obtained this by using the software any2fasta)
cd ../../2020_08_30_run_predictions/data/fasta_gfa/

files=$(ls *fasta | sed 's/.fasta//g')

#run quast, the target the simulated contigs of the strains against the complete genome of the strains, to later obtain the number of contigs that align to each plasmid.
for strains in $files
do
quast -o ../../../2020_09_01_data_analysis/results/quast_output/self_alignments_gfa/${strains} -r ../../../2020_08_25_ecoli_metadata/data/fasta_final/${strains}_genomic.fna -m 1000 -t 8 -i 500 --no-snps --ambiguity-usage all ${strains}.fasta
done
cd ../
done

