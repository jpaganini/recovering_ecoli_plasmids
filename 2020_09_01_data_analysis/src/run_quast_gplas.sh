#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

while getopts c: flag
do
  	case "${flag}" in
                c) conda=${OPTARG};;
        esac
done

source ${conda}/etc/profile.d/conda.sh

##----------------------------

conda activate quast_mmbioit

##----------run quast for gplas predictions --------------------#

files=$(cat ../../2020_08_30_run_predictions/results/benchmark_list.csv)
#files=$(cat ../../2020_08_30_run_predictions/results/missing_strains.csv)


#move to the folder that holds the gplas_predictions 
cd ../../tools/gplas/results

#run quast, the target is aligning each individual prediction to the complete reference genome to then extract the data
for strains in $files
do
all_bins=$(ls ${strains}_bin_*.fasta | sed 's/.fasta//g' | grep -v Unbinned)
for bin in $all_bins
do
quast -o ../../../2020_09_01_data_analysis/results/quast_output/gplas/${strains}/${bin} -r ../../../2020_08_25_ecoli_metadata/data/fasta_final/${strains}_genomic.fna -m 1000 -t 8 -i 500 --no-snps --ambiguity-usage all ${bin}.fasta
done
done
