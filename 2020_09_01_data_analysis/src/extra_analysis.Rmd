---
title: "recovering_ecoli_plasmids_analysis"
output: html_document
---

```{r setup, include=FALSE}

library(rmarkdown)
library(knitr)
library(ggplot2)
library(tidyr)
library(broom)
library(dplyr)
library(readxl)
library(reshape2)
library(tidyverse)
library(igraph)
library(RColorBrewer)
library(RCy3)
library(stringr)
library(Rtsne)
library(umap)

knitr::opts_chunk$set(fig.width=18, fig.height=14)
knitr::opts_chunk$set(echo = TRUE)

```

#1. Analyze characteristics of the benchmarking data-set
#1.1 Create a Supplementary table with the metadata, and a suplmentary table for uploading to PopPUNK

```{r}
important_metadata<-read.csv('../../2020_08_25_ecoli_metadata/results/all_metadata.csv', sep=',', header=TRUE)
benchmark_list<-read.csv('../../2020_08_30_run_predictions/results/benchmark_list.csv', header=FALSE, sep=',')
names(benchmark_list)<-'long_id'


#add a mark for indicating that this will be the strains that will be benchmarked
benchmark_list$benchmark__autocolour<-'yes'

#cross information to get a mark of the strains that will be used for benchmarking
all_metadata_benchmark<-left_join(important_metadata,benchmark_list,by='long_id')

#filter benchmark metadata
benchmark_metadata<-filter(all_metadata_benchmark,benchmark__autocolour=='yes')

#add the information regarding the amount of plasmids that each strain carries
#2. Import the list of strain names and plasmid names
strain_plasmids<-read.csv('../../2020_08_30_run_predictions/data/strains_plasmids.csv', sep=',', header=FALSE)
names(strain_plasmids)<-c('strain','plasmid_id')
#3.Get the amount of plasmids per strain
plasmids_per_strain<-strain_plasmids %>% group_by(strain) %>% summarise(plasmid_count=n())

#4. Cross the information with the rest of the metadata
benchmark_metadata<-left_join(benchmark_metadata,plasmids_per_strain,by=c('long_id'='strain'))
#assign 0 to the NA
benchmark_metadata$plasmid_count[is.na(benchmark_metadata$plasmid_count)]<-'0'

export_benchmark_data<-benchmark_metadata[,-c(21)]
#write.csv(export_benchmark_data,'../results/benchmarking_complete_metadata.csv', row.names = FALSE)

#filter the data to get a microreact table
microreact_metadata<-all_metadata_benchmark[,c(15,17,16,14,18,19,8,21)]
#change names of columns for microreact
names(microreact_metadata)<-c("id","ST__autocolour","PopPUNK_clusters__autocolour","Phylogroup__autocolour","continent__autocolour","host__autocolour","release_date","benchmark__autocolour")
#write.csv(microreact_metadata, '../results/microreact_benchmark.csv', row.names = FALSE)

#Frist, we will analyze in more depth the soruces of the human isolates
#import the data from the homo_sapiens bioprojects. This was obtained by running the script homosapiens_info.sh
homosapiens_classification<-read.csv('../data/bioprojects_data/bioproject_summary.tsv', header=FALSE, sep='\t')
names(homosapiens_classification)<-c('bioproject','description','classification')

#add the information to the homo-sapiens strains
homosapiens_metadata<-filter(benchmark_metadata,new_host=='Homo sapiens')

#cross the information with the bioprojects
homosapiens_final<-inner_join(homosapiens_metadata,homosapiens_classification,by='bioproject')

#filter important columns and merge with the entire meta-data
homosapiens_strain_classification<-homosapiens_final[,c(1,24)]
homosapiens_strain_classification_count<-homosapiens_strain_classification %>% group_by(classification) %>% summarise(count_classification=n())

#join all the metadata with the classificaiton for homo-sapiens strains
benchmark_metadata<-left_join(benchmark_metadata,homosapiens_strain_classification, by='assembly_accession')
benchmark_metadata$classification<-as.character(benchmark_metadata$classification)
benchmark_metadata$classification[is.na(benchmark_metadata$classification)]<-'not_applicable'


```



#1.2 Plot ST-distribuition, host distribuition, continent distribution

```{r}

#make a stacked bar plot for exploring the different sources
#first we will count the source
count_source<-benchmark_metadata %>% group_by(new_host) %>% summarise(count_source=n())
benchmark_metadata<-inner_join(benchmark_metadata,count_source,by='new_host')

ggplot(benchmark_metadata, aes(x=reorder(new_host, -count_source), fill=classification)) + geom_bar(stat='count') +
  scale_fill_manual(values=c("#EC7063","#1E8FE2","#FFC300","#9B9B9B")) +
  theme_minimal()+ ggtitle("Isolation source") +
  theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=28,face='bold'), axis.text.x=element_text(size=22,angle=45,hjust=1),panel.grid.major.x = element_blank(), axis.title.y=element_text(size=30,face="bold"), axis.text.y=element_text(size=18,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +
   xlab('Source') + ylab('nr. of strains') 
 
#count the continents 
count_continent<-benchmark_metadata %>% group_by(continent) %>% summarise(count_continent=n())
#combine it
benchmark_metadata<-inner_join(benchmark_metadata,count_continent,by='continent')
#now we are going to draw it
ggplot(benchmark_metadata, aes(x=reorder(continent, -count_continent))) + geom_bar(stat='count') +
  scale_fill_manual(values=c("#EC7063","#1E8FE2","#FFC300","#9B9B9B")) +
  theme_minimal()+ ggtitle("Geographical Location") +
  theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=28,face='bold'), axis.text.x=element_text(size=22,angle=45, hjust=1),panel.grid.major.x = element_blank(), axis.title.y=element_text(size=30,face="bold"), axis.text.y=element_text(size=18,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +
   xlab('Geographical location') + ylab('nr. of strains') 

#still have to analyze the ST

count_st<-benchmark_metadata %>% group_by(ST) %>% summarise(count_st=n())
st_graph<-count_st
st_graph$ST<-as.character(st_graph$ST)
st_graph$st_group<-ifelse(st_graph$count_st<10,'other',st_graph$ST)
st_graph$st_group<-ifelse(st_graph$ST=='-','N/A',st_graph$st_group )

ggplot(st_graph, aes(x=reorder(st_group, -count_st),y=count_st)) + geom_bar(stat='identity') +
  theme_minimal()+ ggtitle("ST abundance") +
  theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=28,face='bold'), axis.text.x=element_text(size=22,angle=0, vjust=2),panel.grid.major.x = element_blank(), axis.title.y=element_text(size=30,face="bold"), axis.text.y=element_text(size=18,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +
   xlab('ST') + ylab('nr. of strains') 


###--------COUNT THE PHYLOGROUPS-------------------------------------------------####
#get a copy of the complete metadata
all_metadata_benchmark_phylogroups<-all_metadata_benchmark
#count the phylogroups
count_phylogroup<-all_metadata_benchmark_phylogroups %>% group_by(phlyogroup) %>% summarise(count_phylogroup=n())
#combine it
all_metadata_benchmark_phylogroups<-inner_join(all_metadata_benchmark_phylogroups,count_phylogroup,by='phlyogroup')
#replace NA values
all_metadata_benchmark_phylogroups$benchmark__autocolour[is.na(all_metadata_benchmark_phylogroups$benchmark__autocolour)]<-'no'
#rename the column benchmark__autocolour
colnames(all_metadata_benchmark_phylogroups)[21] <- "benchmark_group"
#now we are going to draw it
ggplot(all_metadata_benchmark_phylogroups, aes(x=reorder(phlyogroup, -count_phylogroup), fill=benchmark_group)) + geom_bar(stat='count') +
  scale_fill_manual(values=c("#8E8B8B","#FF9933")) +
  theme_minimal()+ ggtitle("Phylogroup distribution") +
  theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=28,face='bold'), axis.text.x=element_text(size=22,angle=0),panel.grid.major.x = element_blank(), axis.title.y=element_text(size=30,face="bold"), axis.text.y=element_text(size=18,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +  
   xlab('Phylogroups') + ylab('nr. of strains') 

#get the relative abundance for the phylogroups

phylogroup_relative<-all_metadata_benchmark_phylogroups %>% group_by(phlyogroup,benchmark_group ) %>% summarise(count=n()) %>% mutate(perc=count/sum(count))

ggplot(phylogroup_relative, aes(x=reorder(phlyogroup,-count),y=perc*100,fill=benchmark_group)) + geom_bar(stat='identity') +
  theme_classic()+ ggtitle("Relative abundance of phylogroups") +
   scale_fill_manual(values=c("#8E8B8B","#FF9933")) +
theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=28,face='bold'), axis.text.x=element_text(size=22,angle=0),panel.grid.major.x = element_blank(), axis.title.y=element_text(size=30,face="bold"), axis.text.y=element_text(size=18,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +  
   geom_text(aes(label=as.integer(perc*100)),position=position_stack(vjust=0.5),size=8) +
   xlab('Phylogroups') + ylab('relative abundance (%)') + guides(shape = guide_legend(override.aes = list(size = 0.5))) +
  theme(legend.title = element_text(size = 22), legend.text = element_text(size = 18))



###------------------------------ EVALUATE THE AMOUNT OF PLASMIDS PER STRAINS---------------------

count_plasmids_occurence<-benchmark_metadata %>% group_by(plasmid_count) %>% summarise(plasmids_ocurence=n())

count_plasmids_occurence$plasmid_count<-as.numeric(count_plasmids_occurence$plasmid_count)

ggplot(benchmark_metadata, aes(x=as.numeric(plasmid_count))) + geom_bar(stat='count') +
  #scale_fill_manual(values=c("#8E8B8B","#FF9933")) +
  scale_x_continuous(breaks=c(0,1,2,3,4,5,6,7,8,9,10,11)) +
  theme_minimal()+ ggtitle("Number of plasmids per strain") +
  theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=28,face='bold'), axis.text.x=element_text(size=22),panel.grid.major.x = element_blank(), axis.title.y=element_text(size=30,face="bold"), axis.text.y=element_text(size=18,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +
  xlab('nr of plasmids') + ylab('nr. of strains') 

```

#2. This part of the code is designed for prooving the genomic diversity of the plasmids included in the benchmakring data-set

In order to achieve this we did a few things:
1-Get the genomic distances betweenthe plasmids including in the benchmark using MASH (k=21,s=1000)
2-Construct a histogram that shows the distribution of MASH distances
3-Get the genomic distances between all the  plasmids avaialble in the complete genomes of NCBI (3264 plasmids) using MASH (k=21,s=1000)
4- Do a t-SNE analysis to show the diversity of all the plasmids. Colour by benchmark-included or not.


```{r}
#---------------1. MASH distance distribution between plasmids included in the benchmarking data set---------------------#

#1. Import MASH distances matrix for plasmids included on the benchmarking (this distances were obtained by script run_analysis.sh)
distances<-read.table(file='../data/plasmids_mash_distances/plasmid_distances.tab', sep='\t',dec=',')

# Parsing the dataframe 
bins <- distances[,1]
mash_df <- distances[,c(2:ncol(distances))]

# Setting the matrix correctly 
colnames(mash_df) <- bins
rownames(mash_df) <- bins

# Considering the object as a distance matrix 
dist_mash <- as.dist(mash_df)

# Using tidyverse to reestructure the matrix into a data frame 
plot_mash <- as.data.frame(mash_df) %>%
  gather(key = bin, value = mash_distance)

plot_mash$bin_versus <- bins

# Mini viz of the data frame 
head(plot_mash)

# Removing possible pairwise duplicates
plot_mash <- plot_mash[!duplicated(plot_mash),]

# Removing pairwise connections of plasmids against themselves (self-connections resulting in pairwise distances of 0.0)
plot_mash <- subset(plot_mash, plot_mash$bin != plot_mash$bin_versus)

# Creating a new dataframe that will be later for plotting the histogram
weight_graph <- data.frame(From_to = plot_mash$bin,
                           To_from = plot_mash$bin_versus,
                           weight = as.numeric(as.character(plot_mash$mash_distance)))

#plot
threshold_distribution <- ggplot(weight_graph, aes(x=weight)) + 
    geom_histogram(aes(y=..density..),      # Histogram with density instead of count on y-axis
                   binwidth=.015,
                   colour="black", fill="#b5b5b5") +
  geom_density(alpha=.5, fill="#FF9933") +
  theme_minimal()+ ggtitle('Plasmid distance distribution') +
    #geom_vline(xintercept = 0.025, linetype = 'dashed') +
  theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=24,face='bold'), axis.text.x=element_text(size=20),panel.grid.major.x = element_blank(), axis.title.y=element_text(size=24,face="bold"), axis.text.y=element_text(size=20,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +
  xlab('Mash Distance (k=21, s=1000)') + ylab('Density') 
    

threshold_distribution

#-------------------------------2. create a t-SNE of all plasmids to analyze general distribution------------#

#1. Import MASH distances matrix between plasmids and construct an histogram

all_distances<-read.table(file='../data/plasmids_mash_distances/all_plasmid_distances.tab', sep='\t', dec=',')

# Parsing the dataframe 
all_bins <- all_distances[,1]
all_mash_df <- all_distances[,c(2:ncol(all_distances))]

# Setting the matrix correctly 
colnames(all_mash_df) <- all_bins
rownames(all_mash_df) <- all_bins

# Considering the object as a distance matrix 
all_dist_mash <- as.dist(all_mash_df)

#extract the order of the files in the distance matrix
tnse_plasmid_names<-as.data.frame(attr(all_dist_mash,"Labels"))
names(tnse_plasmid_names)<-'plasmid_id'

#------------Now we will start with the tsne part-----------------------------------

#1. We will set the seed, since rtsne relies on a random inisialization of the data
set.seed(123)

#create the tsne dimensionality reduction with perplexity 30
tsne<-Rtsne(all_dist_mash,dims=2, perplexity=30, check_duplicates=TRUE, max_iter=5000)

#----------------get important metadata for visualizing --------------------------------
tsne_metadata<-all_metadata_benchmark[,c('long_id','benchmark__autocolour','new_host','continent','ST','phlyogroup')]
tsne_metadata[is.na(tsne_metadata)]<-'no'

#cross information with the plasmid data
tsne_metadata_plasmids<-inner_join(strain_plasmids,tsne_metadata,by=c('strain'='long_id'))

#order the information according to the tsne data
tsne_metadata_plasmids_ordered<-inner_join(tnse_plasmid_names,tsne_metadata_plasmids,by='plasmid_id')

#---------------------------PLOT TSNE --------------------------------------#

#Before plotting, we gather all relevant data in a single dataframe

plot_tsne_data <- data.frame(x = tsne$Y[,1],
                 y = tsne$Y[,2],
                benchmark_set = tsne_metadata_plasmids_ordered$benchmark__autocolour)

#plot
ggplot(plot_tsne_data, aes(x, y, colour = benchmark_set)) + geom_point(size=3) + 
  theme_light() +
  ggtitle("t-SNE plasmids MASH distances") +
  scale_color_manual(values=c("#8E8B8B","#FF9933")) +
  theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=28,face='bold'), axis.text.x=element_text(size=22,angle=0, hjust=1), axis.title.y=element_text(size=30,face="bold"), axis.text.y=element_text(size=18,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +
   xlab('Dimension 1') + ylab('Dimension 2') 



```

#3.2 Now that we have the cut-off value, we will create the network using igraph

```{r}
filtered_graph <- weight_graph

# The R package igraph considers that lower weights correspond to a distances of 0, which is the opposite to the distance given by Mash
# To bypass this, we reverse the scale 0-1 to 1-0. 
filtered_graph$weight <- 1-filtered_graph$weight

# We keep only distances higher than 0.9
filtered_graph <- subset(filtered_graph, filtered_graph$weight > (1-0.1))

count_plasmids<-filtered_graph %>% group_by(From_to) %>% summarise(count=n())

# Again, we make sure that we don't include self-connections
filtered_graph <- subset(filtered_graph, filtered_graph$From_to != filtered_graph$To_from)



# The column weight will be considered by igraph to draw the width of the edges 
filtered_graph$width <- filtered_graph$weight

# We use the function from igraph to create a network from a dataframe
graph_pairs <- graph_from_data_frame(filtered_graph, directed = FALSE)

# We check that igraph considers the weight (reversed Mash distances) of the connections
is.weighted(graph_pairs)

# We retrieve the name of the vertices in the network
vertices_graph <- V(graph_pairs)


#----- We want to incorporate the network the information regarding  the isolation source-----
#import the metadata of the strains
metadata<-read.csv('../../2020_08_25_ecoli_metadata/results/all_metadata.csv', sep=',')
#we will filter the important columns
filtered_metadata<-metadata[,c(20,19,18,17,16)]

#Since the network includes the names of the plasmids, we need to find the relation between the names of the strains and the name of the plasmids.so...
#import the relations between strains and plasmids names
strains_plasmids<-read.csv('../../2020_08_30_run_predictions/data/strains_plasmids.csv', sep=',', header=FALSE)
names(strains_plasmids)<-c('strain','plasmid_id')
#mix the plasmids with the rest of the metadata
plasmids_metadata<-inner_join(strains_plasmids,filtered_metadata,by=c('strain'='long_id'))

#we will filter only the plasmids that are being included in the benchmark

plasmids_metadata <- subset(plasmids_metadata, plasmids_metadata$plasmid_id %in% bins)
plasmids_metadata_filtered<-plasmids_metadata[,c('plasmid_id','new_host')]

#count how many hosts we have
number_hosts<-plasmids_metadata_filtered %>% group_by(new_host) %>% summarise(st_count=n()) #we have 156 diferent ST groups

# We create using the function brewer.pal.info from the package RColorBrewer a palette with distinct colours
n <- 8
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))

# We assign the colours to the vertices in the network based on the host
V(graph_pairs)$color <- col_vector[plasmids_metadata_filtered$new_host]


# The size of the node is reduced so we can clearly see the structure of the graph
V(graph_pairs)$size <- 2

head(vertices_graph)


#The name of the vertices are important for the following steps but for the visualization it is better if the vertices don't have a name so we avoid overlap between the names and the colour of the nodes can be appreciated 

# We store the name of the vertices in the network so we can put them again in the network 
storing_vertices_names <- V(graph_pairs)$name 
V(graph_pairs)$name <- ''

# We plot the network 
plot(graph_pairs)

#get the density of the network
edge_density(graph_pairs) #0.18, that is pretty dense



```
```{r}
#The orientation of the graph has some randomness, we kept and save the coordinates of one possible orientation so we can always plot the network in the same way


# The orientation is the graph has some randomness, we try to fix it by fixing the seed 
set.seed(123)

plot(graph_pairs)

coords <- layout.fruchterman.reingold(graph_pairs) 

# To have always the same orientation in the plot graph, we stored the coorinates once 
#write.table(coords,'../results/plasmids_network/benchmark_plasmids_network.csv', sep='\t',row.names = FALSE)

# We recover the coordinates of the vertices 
coords <- read.table( '../results/plasmids_network/benchmark_plasmids_network.csv',sep='\t', dec=',', header=TRUE)
coords$V1<-as.numeric(coords$V1)
coords$V2<-as.numeric(coords$V2)
coords <- as.matrix(coords)

# We plot the network 
plot(graph_pairs, layout = coords)

# We create a legend with the assignment of colours based on SC
text_legend <- sort(unique(plasmids_metadata_filtered$new_host))
colours_legend <- col_vector[text_legend]

legend("topleft", legend=text_legend, col = colours_legend , bty = "o", pch=20 , pt.cex = 3, cex = 1.5, horiz = FALSE, inset = c(0.0, 0.5))





```

#Experimenting with visualizin the network on Cytoscape using the package RCy3

```{r}
# We introduce again the name of the vertices in the graph 
V(graph_pairs)$name <- storing_vertices_names

#Try to get the network represented in Cytoscape
cytoscapePing()

createNetworkFromIgraph(graph_pairs,"myIgraph")



```




#2.4 Inspecting the network a bit further

```{r}
# We introduce again the name of the vertices in the graph 

V(graph_pairs)$name <- storing_vertices_names

graph_report <- data.frame(Strain = V(graph_pairs)$name, 
                           Component = as.numeric(as.character(components(graph_pairs)$membership)))

# Number of vertices in the graph 
nrow(graph_report)

# Number of vertices per component, only components with > 2 nodes 
graph_report %>%
  group_by(Component) %>%
  count() %>%
  filter(n > 2) 

# The same information as before displayed in a different way 
components(graph_pairs)

#got 20 different network components. Out of which, component number is the one that has many vertexes (682).
#so, we are going to explore this a bit better

# returns a list of three graphs
# dg is the object in which the different components in the graph are considered as subgraphs 
dg <- decompose.graph(graph_pairs) 

graph_interest <- dg[[1]]

V(graph_interest)$size <- 2

head(graph_interest)


#The name of the vertices are important for the following steps but for the visualization it is better if the vertices don't have a name so we avoid overlap between the names and the colour of the nodes can be appreciated 

# We store the name of the vertices in the network so we can put them again in the network 
#graph_interest_names <- V(graph_interest)$name 
#V(graph_interest)$name <- ''

#plot(graph_interest)

graph_louvain <- cluster_louvain(graph_interest)
modularity(graph_louvain)


info_louvain <- data.frame(Cluster = graph_louvain$membership,
                            Strain = graph_louvain$names)

```



#3.1 Construct a Network of plasmid sequences based on their genomic similarity, FOR ALL AVAILABLE PLASMIDS

#3.1 Find the cut-off value for stablishing an edge to draw the network

```{r}
#1. Import MASH distances matrix between plasmids and construct an histogram

all_distances<-read.table(file='../data/plasmids_mash_distances/all_plasmid_distances.tab', sep='\t', dec=',')

# Parsing the dataframe 

all_bins <- all_distances[,1]
all_mash_df <- all_distances[,c(2:ncol(all_distances))]

# Setting the matrix correctly 
colnames(all_mash_df) <- all_bins
rownames(all_mash_df) <- all_bins

# Considering the object as a distance matrix 
all_dist_mash <- as.dist(all_mash_df)

# Using tidyverse to reestructure the matrix into a data frame 
all_plot_mash <- as.data.frame(all_mash_df) %>%
  gather(key = bin, value = mash_distance)

all_plot_mash$bin_versus <- all_bins

# Mini viz of the data frame 
head(all_plot_mash)

# Removing possible pairwise duplicates
all_plot_mash <- all_plot_mash[!duplicated(all_plot_mash),]

# Removing pairwise connections of plasmids against themselves (self-connections resulting in pairwise distances of 0.0)
all_plot_mash <- subset(all_plot_mash, all_plot_mash$bin != all_plot_mash$bin_versus)



# Creating a new dataframe that will be later used in igraph. We create a column called weight, which corresponds to the Mash distance 

all_weight_graph <- data.frame(From_to = all_plot_mash$bin,
                           To_from = all_plot_mash$bin_versus,
                           weight = as.numeric(as.character(all_plot_mash$mash_distance)))


all_threshold_distribution <- ggplot(all_weight_graph, aes(x=weight)) + 
    geom_histogram(aes(y=..density..),      # Histogram with density instead of count on y-axis
                   binwidth=.005,
                   colour="black", fill="white") +
    #geom_vline(xintercept = 0.025, linetype = 'dashed') +
    geom_density(alpha=.5, fill="lightblue") + 
    labs(x = 'Mash Distance (k = 21, s = 1,000)')

all_threshold_distribution


#most of the values of MASH distances are 1, because there is a great diversity of plasmids. So, we remove all values that are bigger than 0.4, so we can have a better visualization to decide for a cut-off value.
all_weight_graph_filtered<-subset(all_weight_graph,all_weight_graph$weight<=0.4)

#now plot again
all_threshold_distribution_filtered <- ggplot(all_weight_graph_filtered, aes(x=weight)) + 
    geom_histogram(aes(y=..density..),      # Histogram with density instead of count on y-axis
                   binwidth=.005,
                   colour="black", fill="white") +
    geom_vline(xintercept = 0.08, linetype = 'dashed') +
    geom_density(alpha=.5, fill="lightblue") + 
    labs(x = 'Mash Distance (k = 21, s = 1,000)')

all_threshold_distribution_filtered
```

#3.2 Now we will build the Network

```{r}
all_filtered_graph <- all_weight_graph

# The R package igraph considers that lower weights correspond to a distances of 0, which is the opposite to the distance given by Mash
# To bypass this, we reverse the scale 0-1 to 1-0. 
all_filtered_graph$weight <- 1-all_filtered_graph$weight


# We keep only distances higher than 0.9
all_filtered_graph <- subset(all_filtered_graph, all_filtered_graph$weight > (1-0.08))

# Again, we make sure that we don't include self-connections
all_filtered_graph <- subset(all_filtered_graph, all_filtered_graph$From_to != all_filtered_graph$To_from)

all_count_plasmids<-all_filtered_graph %>% group_by(From_to) %>% summarise(count=n())

# The column weight will be considered by igraph to draw the width of the edges 
all_filtered_graph$width <- all_filtered_graph$weight

# We use the function from igraph to create a network from a dataframe
all_graph_pairs <- graph_from_data_frame(all_filtered_graph, directed = FALSE)

# We check that igraph considers the weight (reversed Mash distances) of the connections
is.weighted(all_graph_pairs)

# We retrieve the name of the vertices in the network
all_vertices_graph <- V(all_graph_pairs)


#----- We want to incorporate the network the information regarding  the isolation source-----
#import the metadata of the strains
all_metadata<-read.csv('../../2020_08_25_ecoli_metadata/results/all_metadata.csv', sep=',')
#we will filter the important columns
all_filtered_metadata<-all_metadata[,c(20,19,18,17,16)]

#Since the network includes the names of the plasmids, we need to find the relation between the names of the strains and the name of the plasmids.so...
#import the relations between strains and plasmids names
strains_plasmids<-read.csv('../../2020_08_30_run_predictions/data/strains_plasmids.csv', sep=',', header=FALSE)
names(strains_plasmids)<-c('strain','plasmid_id')
#mix the plasmids with the rest of the metadata
all_plasmids_metadata<-inner_join(strains_plasmids,all_filtered_metadata,by=c('strain'='long_id'))

#we will filter only the plasmids that are being included in the benchmark

all_plasmids_metadata <- subset(all_plasmids_metadata, all_plasmids_metadata$plasmid_id %in% all_bins)
all_plasmids_metadata_filtered<-all_plasmids_metadata[,c('plasmid_id','new_host')]

#count how many hosts we have
all_number_hosts<-all_plasmids_metadata_filtered %>% group_by(new_host) %>% summarise(host_count=n()) 

# We create using the function brewer.pal.info from the package RColorBrewer a palette with distinct colours
n <- 9
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))

# We assign the colours to the vertices in the network based on the host
V(all_graph_pairs)$color <- col_vector[all_plasmids_metadata_filtered$new_host]


# The size of the node is reduced so we can clearly see the structure of the graph
V(all_graph_pairs)$size <- 1.5




#The name of the vertices are important for the following steps but for the visualization it is better if the vertices don't have a name so we avoid overlap between the names and the colour of the nodes can be appreciated 

# We store the name of the vertices in the network so we can put them again in the network 
all_storing_vertices_names <- V(all_graph_pairs)$name 
V(all_graph_pairs)$name <- ''

# We plot the network 
#plot(all_graph_pairs)
```

# Experimental block, loop for creating different data-frames with different mash distances values cut-off  to evaluate the performance of oslom
```{r}
#1. create a list for the potential cut-off values
mash_cut_off<-seq(0.005, 0.4, by=0.005)

#2. get my original value
unfiltered_network <- data.frame(From_to = all_plot_mash$bin,
                           To_from = all_plot_mash$bin_versus,
                           weight = as.numeric(as.character(all_plot_mash$mash_distance)))

#3. Reverse the values of mash distances since networks softwares like oslo have reversed values. 1=max similiraty
unfiltered_network$weight <- 1-unfiltered_network$weight
#4.Renamed the from_to and to_from columns with only numbers for oslom to be able to process it.
#There is a problem, when renaming, we found two different plasmid codes that provide the same output
#therefore, we have the make the following changes in the names of plasmids, which will be reversed later
#AP022165.1 -> AP022165.2
#AP022166.1 -> AP022166.2
#AP022167.1 -> AP022167.1
unfiltered_network$From_to <- gsub('AP022165.1', 'AP022165.2', unfiltered_network$From_to)
unfiltered_network$To_from <- gsub('AP022165.1', 'AP022165.2', unfiltered_network$To_from)

unfiltered_network$From_to <- gsub('AP022166.1', 'AP022166.2', unfiltered_network$From_to)
unfiltered_network$To_from <- gsub('AP022166.1', 'AP022166.2', unfiltered_network$To_from)

unfiltered_network$From_to <- gsub('AP022167.1', 'AP022167.2', unfiltered_network$From_to)
unfiltered_network$To_from <- gsub('AP022167.1', 'AP022167.2', unfiltered_network$To_from)

#Now we can rename everything
unfiltered_network$From_to_renamed<-str_sub(unfiltered_network$From_to,-8,-1)
unfiltered_network$From_to_renamed <- gsub('[.]', '', unfiltered_network$From_to_renamed)
unfiltered_network$To_from_renamed<-str_sub(unfiltered_network$To_from,-8,-1)
unfiltered_network$To_from_renamed <- gsub('[.]', '', unfiltered_network$To_from_renamed)

#Now we check that we dont have repeated names
check_one<-unfiltered_network %>% group_by(From_to) %>% summarise(count=n())
check_two<-unfiltered_network %>% group_by(From_to_renamed) %>% summarise(count=n())
#We have 3248 connections for every one, so that means that is good

#filter the interesting columns
final_set<-unfiltered_network[,c(4,5,3)]


for (cut_off in mash_cut_off) {
  final_table <- subset(final_set, final_set$weight > (1-cut_off))
  name<-paste('../results/plasmids_network/filtered_network',cut_off,sep='_')
  final_name<-paste(name,'dat',sep='.')
  print(final_name)
  write.table(final_table,file=final_name,sep='\t',col.names = FALSE, row.names = FALSE,quote=FALSE)
}


#Now I will generate 4 random numbers that will be used as initial number of seeds for oslom

set.seed(45) # random number will generate from 5
FourRandomNumbers <- sort(sample.int(300, 4))
FourRandomNumbers

#37, 39, 58, 224


```




#Experimenting with visualizin the network on Cytoscape using the package RCy3

```{r}
# We introduce again the name of the vertices in the graph 
V(all_graph_pairs)$name <- all_storing_vertices_names

#Try to get the network represented in Cytoscape
cytoscapePing()

createNetworkFromIgraph(all_graph_pairs,"myIgraph")



```


#3.3 Looking at the network in a bit more detail

```{r}
#V(all_graph_pairs)$name <- all_storing_vertices_names

all_graph_report <- data.frame(Strain = V(all_graph_pairs)$name, 
                           Component = as.numeric(as.character(components(all_graph_pairs)$membership)))

# Number of vertices in the graph 
nrow(all_graph_report)

# Number of vertices per component, only components with > 2 nodes 
all_components<- all_graph_report %>%
  group_by(Component) %>%
  count() %>%
  filter(n > 2) 

# The same information as before displayed in a different way 
components(all_graph_pairs)

edge_density(all_graph_pairs)

# dg is the object in which the different components in the graph are considered as subgraphs 
all_dg <- decompose.graph(all_graph_pairs) 

all_graph_interest <- all_dg[[7]]

V(all_graph_interest)$size <- 2

# This is the graph if we only plot that particular component 
plot(all_graph_interest)



```

#4. Construct a t-sne visualization for all the plasmid sequences
```{r}

#1. Import MASH distances matrix between plasmids and construct an histogram

all_distances<-read.table(file='../data/plasmids_mash_distances/all_plasmid_distances.tab', sep='\t', dec=',')

# Parsing the dataframe 
all_bins <- all_distances[,1]
all_mash_df <- all_distances[,c(2:ncol(all_distances))]

# Setting the matrix correctly 
colnames(all_mash_df) <- all_bins
rownames(all_mash_df) <- all_bins

# Considering the object as a distance matrix 
all_dist_mash <- as.dist(all_mash_df)

#extract the order of the files in the distance matrix
tnse_plasmid_names<-as.data.frame(attr(all_dist_mash,"Labels"))
names(tnse_plasmid_names)<-'plasmid_id'

#------------Now we will start with the tsne part-----------------------------------

#1. We will set the seed, since rtsne relies on a random inisialization of the data
set.seed(123)

#create the tsne dimensionality reduction with perplexity 30
tsne<-Rtsne(all_dist_mash,dims=2, perplexity=30, check_duplicates=TRUE, max_iter=5000)

#----------------get important metadata for visualizing --------------------------------
tsne_metadata<-all_metadata_benchmark[,c('long_id','benchmark__autocolour','new_host','continent','ST','phlyogroup')]
tsne_metadata[is.na(tsne_metadata)]<-'no'

#cross information with the plasmid data
tsne_metadata_plasmids<-inner_join(strain_plasmids,tsne_metadata,by=c('strain'='long_id'))

#order the information according to the tsne data
tsne_metadata_plasmids_ordered<-inner_join(tnse_plasmid_names,tsne_metadata_plasmids,by='plasmid_id')

#---------------------------PLOT TSNE --------------------------------------#

#Before plotting, we gather all relevant data in a single dataframe

plot_tsne_data <- data.frame(x = tsne$Y[,1],
                 y = tsne$Y[,2],
                benchmark_set = tsne_metadata_plasmids_ordered$benchmark__autocolour)

#plot
ggplot(plot_tsne_data, aes(x, y, colour = benchmark_set)) + geom_point(size=3) + 
  theme_light() +
  ggtitle("t-SNE plasmids MASH distances") +
  scale_color_manual(values=c("#8E8B8B","#FF9933")) +
  theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=28,face='bold'), axis.text.x=element_text(size=22,angle=0, hjust=1), axis.title.y=element_text(size=30,face="bold"), axis.text.y=element_text(size=18,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +
   xlab('Dimension 1') + ylab('Dimension 2') 


#-----------------------------------------------------------------------------------------------------------#
#try tsne analysis with a perplexity of 50
tsne_perp_50<-Rtsne(all_dist_mash,dims=2, perplexity=50, check_duplicates=TRUE, max_iter=5000)

plot_tsne_data_perp_50 <- data.frame(x = tsne_perp_50$Y[,1],
                 y = tsne_perp_50$Y[,2],
                benchmark_set = tsne_metadata_plasmids_ordered$benchmark__autocolour)

ggplot(plot_tsne_data_perp_50, aes(x, y, colour = benchmark_set)) + geom_point(size=3) + 
  theme_light() +
  ggtitle("t-SNE plasmids MASH distances") +
  scale_color_manual(values=c("#8E8B8B","#FF9933")) +
  theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=28,face='bold'), axis.text.x=element_text(size=22,angle=0, hjust=1), axis.title.y=element_text(size=30,face="bold"), axis.text.y=element_text(size=18,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +
   xlab('Dimension 1') + ylab('Dimension 2') 


#try tsne analysis with a perplexity of 100
tsne_perp_100<-Rtsne(all_dist_mash,dims=2, perplexity=100, check_duplicates=TRUE, max_iter=5000)

plot_tsne_data_perp_100 <- data.frame(x = tsne_perp_100$Y[,1],
                 y = tsne_perp_100$Y[,2],
                benchmark_set = tsne_metadata_plasmids_ordered$benchmark__autocolour)

ggplot(plot_tsne_data_perp_100, aes(x, y, colour = benchmark_set)) + geom_point(size=3) + 
  theme_light() +
  ggtitle("t-SNE plasmids MASH distances") +
  scale_color_manual(values=c("#8E8B8B","#FF9933")) +
  theme(plot.title = element_text(size=38,hjust = 0.5,face='bold'),axis.title.x = element_text(size=28,face='bold'), axis.text.x=element_text(size=22,angle=0, hjust=1), axis.title.y=element_text(size=30,face="bold"), axis.text.y=element_text(size=18,angle=0), legend.title = element_text(size = 22), legend.text = element_text(size = 20)) +
   xlab('Dimension 1') + ylab('Dimension 2') 


```


