#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

while getopts c: flag
do
  	case "${flag}" in
                c) conda=${OPTARG};;
        esac
done

source ${conda}/etc/profile.d/conda.sh

#create environments
conda env create -f ../envs/mash_mmbioit.yml
conda env create -f ../envs/abricate_mmbioit.yml
conda env create -f ../envs/quast_mmbioit.yml
conda env create -f ../envs/any2fasta_mmbioit.yml
conda env create -f ../envs/plascad_mmbioit.yml

#get a list of strains that will be used for benchmarking
files=$(cat ../../2020_08_30_run_predictions/results/benchmark_list.csv | sed 's/"//g')


##-----------------1. RUN MASH FOR ALL THE PLASMIDS PRESENT IN OUR BENCHMARKING DATA-SET -------------------------##

#1. Run mash to get the genomic distances between the strains
#activate environment
conda activate mash_mmbioit

./run_mash.sh


##-----------------2. RUN ABRICATE TO GET THE ATB-R genes in PLASMIDS # dont have to change this-------------------------##

#make a directory to hold the results of resfinder

#activate environment
conda activate abricate_mmbioit

./run_abricate.sh


####---------3. RUN ABRICATE for plasmid predictions

./run_abricate_for_bins.sh



##----------------4. Gather all the plasmid sequences and their respective lengths into a single file, which will be processed in R  #RE-RUN----------##

cd ../

for strains in $files
do
cat ../../2020_08_30_run_predictions/data/separated_genomes/${strains}/${strains}_plasmids_length.csv | sed 's/>//g' >> plasmids_lenghts.csv
done

awk -F' |,' '{print $1 "," $NF}' plasmids_lenghts.csv >> benchmarking_plasmids_lenght.csv 

##--------------5. Run R script for obtaining metadata--------------------------------------------------###

#activate r environment
conda activate r_codes_mmbioit

cd ../src

Rscript get_benchmark_metadata.R

##----------------6. Run scripts for doing the alignments using QUAST------------------------------------##

conda activate quast_mmbioit

./run_quast_fishing.sh
./run_quast_gplas.sh
./run_quast_hyasp.sh
./run_quast_mob.sh
./run_quast_scapp.sh
./run_quast_spades.sh


##------------7. Run script for doing self-alignemnt of contigs for tools that use FASTA files--------------------###
./run_quast_self.sh

##---------------8. Run scripts for doing self alignments of graphs------------------##
#1. First we will convert the gfa files to fasta files using any2fasta
conda activate any2fasta_mmbioit
./run_any2fasta.sh

#2. Now we will run quast aligning the converted files to the closed references

conda activate quast_mmbioit

./run_quast_self_gfa.sh


##---------------9. Run script for counting the amount of predictions that each tool makes ---------##

./gather_plasmids_counts.sh

##-----------10. Run plascad -------------------------------#

conda activate plascad_mmbioit

./run_plascad.sh


##-----------------11. Run scripts for gathering and classifying the results from QUAST -RE-RUN THIS -------------###
conda activate r_codes_mmbioit

python gather_quast_results_fishing.py
python gather_quast_results_gplas.py
python gather_quast_results_hyasp.py
python gather_quast_results_mob.py
python gather_quast_results_scapp.py
python gather_quast_results_self.py
python gather_quast_results_spades.py
python gather_quast_results_self_gfa.py


###------ 12. Run scripts for gathering metadata for human sources-------
conda activate ncbi_download_mmbioit
./homosapiens_info.sh  #the output file from this script will be manually modified
