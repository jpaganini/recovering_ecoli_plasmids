#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

while getopts c: flag
do
  	case "${flag}" in
                c) conda=${OPTARG};;
        esac
done

source ${conda}/etc/profile.d/conda.sh

##----------------------------

#this will have to be change to any2fasta_mmbioit
conda activate any2fasta

##-----------------------------

#1. Create a directory for storing the results

mkdir ../../2020_08_30_run_predictions/data/fasta_gfa

#1. Move to the appropiate folder

cd ../../2020_08_30_run_predictions/data/graphs

#2. Get a list of all the files

files=$(ls *gfa | sed 's/.gfa//g')

#3. Run any2fasta looping thru this list

for strains in $files
do
any2fasta ${strains}.gfa > ../fasta_gfa/${strains}.fasta
done
