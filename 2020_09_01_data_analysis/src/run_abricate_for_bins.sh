#!/bin/bash

##1 ----- We will get a file that contains the paths to the bins for which we will run abricate.
#move to the tools directory
cd ../../tools/
#move to FishingForPlasmids directory
cd FishingForPlasmids
#get the paths to the outputs
find $(pwd) -type f | grep .fasta | grep FFP_output >> ../../2020_09_01_data_analysis/results/predicted_bins_paths.txt

#move to the gplas directory
cd ../gplas
#get the paths to the bins
find $(pwd) -type f | grep .fasta | grep results | grep -v Unbinned | grep -v installation >> ../../2020_09_01_data_analysis/results/predicted_bins_paths.txt

#move to the Hyasp directory
cd ../hyasp
#get the paths
find $(pwd) -type f | grep .fasta | grep bin >> ../../2020_09_01_data_analysis/results/predicted_bins_paths.txt

#move to the mob-suite directory
cd ../mob_predictions
#get the paths to the predictions
find $(pwd) -type f | grep .fasta | grep plasmid_ >> ../../2020_09_01_data_analysis/results/predicted_bins_paths.txt

#move to the scapp directory
cd ../scapp_predictions
#get path to the predictions
find $(pwd) -type f | grep .fasta | grep _RNODE_ >> ../../2020_09_01_data_analysis/results/predicted_bins_paths.txt

#move to the spades directory
cd ../spades_predictions
#get the paths to the predictions
find $(pwd) -type f | grep .fasta | grep _component_ >> ../../2020_09_01_data_analysis/results/predicted_bins_paths.txt

##-----------------. RUN ABRICATE TO GET THE ATB-R genes in the bins -------------------------##

cd ../../2020_09_01_data_analysis/data/atbr_predictions

#Run abricate
abricate --fofn ../../results/predicted_bins_paths.txt > all_bins_atbr_content.tsv

