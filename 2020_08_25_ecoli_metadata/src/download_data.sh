#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

while getopts c: flag
do
  	case "${flag}" in
                c) conda=${OPTARG};;
        esac
done

source ${conda}/etc/profile.d/conda.sh

#create directory structure
mkdir ../data/fasta
mkdir ../data/compressed
mkdir ../data/accessions
mkdir ../results

#create environments
conda env create -f ../envs/ncbi_download_mmbioit.yml
conda env create -f ../envs/r_codes_mmbioit.yml
conda env create -f ../envs/Clermon_mmbioit.yml
conda env create -f ../envs/mlst_mmbioit.yml
conda env create -f ../envs/poppunk_v12_mmbioit.yml


##----------------------------1. Download Data from NCBI----------------------------------------##

conda activate ncbi_download_mmbioit

#extract ftp address to download the data from the file 2020_08_25_ecoli_list.csv
cd ../data

cat 2020_08_25_ecoli_list.csv | cut -f 22 | sed '1d' > ftp_temp.txt

awk '{FS="/";print $0"/"$10"_genomic.fna.gz";}' ftp_temp.txt > ftp.txt

links=$(cat ftp.txt)

for strains in $links
do
wget ${strains} -P compressed/
done

cd compressed/

genomes=$(ls *gz | sed 's/.gz//g')

for things in $genomes
do
gunzip -c ${things}.gz > ../fasta/${things}
done

###--------------------------2. Gather extra metadata using Entrez utilities--------------------------------------------

#get the assembly codes to gather the metadata
cd ../
cat 2020_08_25_ecoli_list.csv | cut -f 1 | sed '1d' > 2020_08_25_ncbi_accessions.csv

#start gathering the data
accessions=$(cat 2020_08_25_ncbi_accessions.csv)

echo 'gathering metadata, probably youll get enough time to make yourself a tea... or two...'

for strains in $accessions:
do
esearch -db assembly -query ${strains} | elink -target nuccore | efetch -format gb  > accessions/${strains}.txt
done

cd accessions

files=$(ls *txt | sed 's/.txt//g')


for strain in ${files[@]}
do
  	country=$(grep '/country' ${strain}.txt | cut -f 1 -d : | sort -u | sed -z "s/\n//g" | cut -f 2 -d = | sed -z 's/"//g' | sed "s/,/;/g" )
        source=$(grep '/isolation_source' ${strain}.txt | cut -f 1 -d : | sort -u | sed -z "s/\n//g" | cut -f 2 -d = | sed -z 's/"//g' | sed "s/,/;/g" )
        host=$(grep '/host' ${strain}.txt | cut -f 1 -d : | sort -u | sed -z "s/\n//g" | cut -f 2 -d = | sed -z 's/"//g' | sed "s/,/;/g" )
        seq_tech=$(grep 'Sequencing Technology' ${strain}.txt | sed 's/: /:/g' | cut -f 3 -d : | sort -u | sed "s/,/;/g" )
        echo ${strain},${seq_tech},${country},${host},${source} >> ../2020_08_25_eutils_metadata.csv
done


###---------------------------------3. Get strains that have been sequenced by Long-Read or Hybrid Approach----------------------------##

conda activate r_codes_mmbioit
cd ../../src

echo "Filtering strains that we sequenced by Long-Reads"

Rscript get_long_reads.R

Move long read fasta to a different folder

cd ../results

long_reads=$(cat long_read_assembly_codes.csv | sed 's/"//g')

mkdir ../data/fasta_long

cd ../data/fasta

for sequences in $long_reads
do
mv ${sequences}* ../fasta_long/
done



#------------------------4. Detect phylogroups using clermonTyping tool ----------------------------------

cd ..

#echo "Downloading ClermonTyping tool"
#git clone https://github.com/A-BN/ClermonTyping.git

conda activate Clermon_mmbioit

cd fasta_long

input=$(ls -1 * | sed -z 's/\n/@/g' | sed 's/.$//')

#run phylogroups predictions using Clermontyping tool
echo "Running ClermonTyping tool"
../ClermonTyping/clermonTyping.sh --fasta ${input} --name clermon_output

#move output folder to the data folder
mv clermon_output ../

#Separate strains that phylogroups are unknown or that potentially belong to a cryptic escherichia clade
cd ../clermon_output

ecoli_codes=$(cat clermon_output_phylogroups.txt | grep -v 'Unknown' | grep -v 'clade' | cut -f 1)

mkdir ../fasta_final

cd ../fasta_long

for strains in $ecoli_codes
do
mv ${strains} ../fasta_final/
done



#-------------------------5. Run Analysis of ST----------------------------------------

conda activate mlst_mmbioit

cd ../fasta_final

mlst --scheme ecoli *fna > ../mlst_all.tsv


#------------------------6. Run PopPUNK-------------------------------------------------

#Remove two extra strains because they have a very divergent accesory genome. -accesory distance > 0.5. Don't seem to be E. coli according to PopPUNK clasification.
#GCA_013894235.1_ASM1389423v1_genomic.fna
#GCA_013817505.1_ASM1381750v1_genomic.fna

mv GCA_013894235.1_ASM1389423v1_genomic.fna ../fasta_long
mv GCA_013817505.1_ASM1381750v1_genomic.fna ../fasta_long

conda activate poppunk_v12_mmbioit

mkdir ../poppunk_output

ls *fna > strain_list.txt

echo "Creating PopPUNK database"
poppunk --create-db --r-files strain_list.txt --output ../poppunk_output/all_ecoli_v12_k15_1e4 --threads 8 --sketch-size 10000 --full-db --plot-fit 5 --min-k 15

cd ../poppunk_output

echo 'Fitting PopPUNK model'
poppunk --fit-model --distances all_ecoli_v12_k15_1e4/all_ecoli_v12_k15_1e4.dists --output K6 --cytoscape --microreact --threads 8 --ref-db all_ecoli_v12_k15_1e4 --K 6

#-----------------------7. Run R-code for organizing metadata--------------------------------
conda activate r_codes_mmbioit

cd ../../src

echo "running last rcode"
Rscript 2020_08_25_ecoli_metadata.R
