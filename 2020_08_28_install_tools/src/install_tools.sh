#!/bin/bash

#Pass the miniconda3 installation directory with the -c flag.

while getopts c: flag
do
  	case "${flag}" in
                c) conda=${OPTARG};;
        esac
done

source ${conda}/etc/profile.d/conda.sh

#create environments
conda env create -f ../envs/gplas_mmbioit.yml
conda env create -f ../envs/fishing_plasmids_mmbioit.yml
conda env create -f ../envs/scapp_mmbioit.yml
conda env create -f ../envs/spades_mmbioit.yml
conda env create -f ../envs/mob-suite_mmbioit.yml
conda env create -f ../envs/hyasp_mmbioit.yml

#---------------1. Configure mob-suite --------------------------------------------------
conda activate mob-suite_mmbioit
echo "Downloading mob-suite data-base, this process might take several hours..."
mob-init

#---------------2. Configure gplas -------------------------------------------------

conda activate gplas_mmbioit
mkdir ../../tools

#get the tool from the repository
cd ../../tools
git clone https://gitlab.com/sirarredondo/gplas.git
cd gplas/
./gplas.sh -i test/faecium_graph.gfa -c mlplasmids -s 'Enterococcus faecium' -n 'installation'

#-----3. Install and configure Fishing for plasmids-------------------#

conda activate fishing_plasmids_mmbioit

#change directory to tools

cd ../  

#Download the git repository

git clone https://github.com/Visser-M/FishingForPlasmids.git

#Run all the necesary things for Installing the tool

#1. Creating directories
cd FishingForPlasmids
mkdir data
cd data
mkdir assemblyDir blast_EcPlGe blast_pFinder FFP_output pmlst_out

#2. Install pmlst and pmlst_db
mkdir ../scripts/pMLST
cd ../scripts/pMLST

git clone https://bitbucket.org/genomicepidemiology/pmlst.git
git clone https://bitbucket.org/genomicepidemiology/pmlst_db.git

cd pmlst_db
pMLST_DB=$(pwd)
python3 INSTALL.py kma_index

#3.Uncompressed the database

unzip ../../../../../2020_08_28_install_tools/databases/fishing_plasmids_db.zip -d ../../../blast_db/

#-------------------4. Configure hyasp -------------------------------------

conda activate hyasp_mmbioit

cd ../../../../

#clone the repository

git clone https://github.com/cchauve/hyasp.git
cd hyasp
#Install tool
echo "Installing HyAsp"
python setup.py sdist
pip install dist/HyAsP-1.0.0.tar.gz

##create hyasp default database from ncbi data --  This step will take a considerable amount of time

echo 'creating HyAsP database, this process will take a few hours... yes... hours...'
cd databases/
hyasp create ncbi_database_genes.fasta -p plasmids.csv -b ncbi_blacklist.txt -d -l 500 -m 100 -t GenBank

