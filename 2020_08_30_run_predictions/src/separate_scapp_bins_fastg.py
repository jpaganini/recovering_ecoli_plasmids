# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import sys
import os
import glob
import fastaparser
import re

wd=os.path.dirname(os.path.realpath(__file__))
os.chdir(wd)
os.chdir('../../tools/scapp_predictions')
genomes=glob.glob('GCA*')


def organize_genome(input_folder):
    strain=input_folder
    components={}

    #Parse fasta files in quick way a quick way.
    with open(input_folder+"/"+strain+".confident_cycs.fasta") as fasta_file:
        parser = fastaparser.Reader(fasta_file, parse_method='quick')
        #loop thru the headers and create a dictionary in which the keys are the name of the RNODES, 
        #and the values will be the set of edges to extract from the fastg file
        for seq in parser:     # seq is a named tuple('Fasta', ['header', 'sequence'])
            plasmid_bin=seq.header.replace('>','')
            plasmid_bin=plasmid_bin.split('_')[0:2]
            plasmid_bin='_'.join(plasmid_bin)
            components[plasmid_bin]=''
            #print(components)
        fasta_file.close()
        
    #Open the file that contains the information with respect to the paths in the graph,
    #and extract the nodes     
    with open(input_folder+"/intermediate_files/"+strain+".cycs.paths.txt") as path_file:
        all_lines=path_file.readlines()
        for bins in components.keys():
            i=0
            for line in all_lines:
                i+=1
                if bins in line:
                    edges=all_lines[i+1].rstrip()
                    edges=edges.replace(']','')
                    edges=edges.replace('[','')
                    edges=edges.replace(' ','')
                    edges=edges.split(',')
                    components[bins]=edges
                else:
                    continue
    #print(components)             
    #Now I will gather the fasta information from the fastgfile
    with open('../../2020_08_30_run_predictions/data/fastg/'+strain+'.fastg') as fastg_file:
        fastg_parser=fastaparser.Reader(fastg_file, parse_method='quick')
        for bins in components:
            with open(input_folder+'/'+strain+'_'+bins+'.fasta', 'a+') as bin_file:               
                for edge in components[bins]:
                    edge='>EDGE_'+edge
                    for seq in fastg_parser:
                        if edge in seq.header:
                            bin_file.write(seq.header)
                            bin_file.write('\n')
                            bin_file.write(seq.sequence)
                            bin_file.write('\n')
                            break     
                               
       
for files in genomes:
    try:
        organize_genome(files)
    except:
        continue

    
