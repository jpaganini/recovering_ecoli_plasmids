#!/bin/bash

#1- Get the list of the biosamples Id to check if we have the reads that will later be used to run the alignments

biosamples=$(cat ../../2020_08_25_ecoli_metadata/results/all_metadata.csv | cut -f 3 -d , | sed 's/"//g' | grep -v biosample)

#2. Now, we will make a look to get the reads identifiers

for biosample in $biosamples
do
sra_accession=$(esearch -db biosample -query "${biosample}" | elink -target sra | efetch -format runinfo  | grep 'Illumina\|ILLUMINA' | cut -f 1 -d ,)
echo ${biosample},${sra_accession} >> ../results/sra_accessions_list_all.csv
done



