# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 18:18:03 2020

@author: jpaganini
"""

import sys
import os
import csv

wd=os.path.dirname(os.path.realpath(__file__))
os.chdir(wd)

os.chdir('../../tools/hyasp/predictions')

strains=os.listdir()
predictions_directory=os.getcwd()

for isolate in strains:
    strain_directory=predictions_directory+'/'+isolate
    os.chdir(strain_directory)
    with open(strain_directory+'/plasmid_bins_putative.csv','r') as bin_list:
        i=1
        for line in bin_list:
            line = line.strip('\n')
            line = line.strip('\r')
            bins=list(line.split(','))
            for sequence in bins:
                cmd="grep -A1 "+'"'+sequence+'\>" putative_plasmid_contigs.fasta '+'>> bin_'+str(i)+'.fasta'
                print(cmd)
                os.system(cmd)
            i+=1
    bin_list.close()
         

