#!/bin/bash

#make directory for holding the bash scripts
mkdir plasmidspades_slurm_jobs_test_memory

#make directory for holding the results
mkdir ../../tools/spades_predictions


#get a list of the files
files=$(cat ../results/benchmark_list.csv)

#create slurm scripts

for strains in $files
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate spades_mmbioit
cd ../../tools/spades_predictions
plasmidspades.py --only-assembler -1 ../../2020_08_30_run_predictions/data/trimmed_reads/${strains}_R1.fq -2 ../../2020_08_30_run_predictions/data/trimmed_reads/${strains}_R2.fq -o ${strains}" > plasmidspades_slurm_jobs_test_memory/${strains}.sh
done

#Run the scripts
cd plasmidspades_slurm_jobs_test_memory
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=4:00:00 --mem=4G ${slurm}
done

