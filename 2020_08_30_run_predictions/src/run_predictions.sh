#!/bin/bash
 
#Pass the miniconda3 installation directory with the -c flag.

while getopts c: flag
do
  	case "${flag}" in
                c) conda=${OPTARG};;
        esac
done

source ${conda}/etc/profile.d/conda.sh

conda env create -f ../envs/trimming_mmbioit.yml
conda env create -f ../envs/seqtk_mmbioit.yml
conda env create -f ../envs/sra_tools_mmbioit.yml

mkdir ../results

#-----------------1. Get the strains that will be used for benchmarking ------------------------------------------
#The strains should be absent from the databases of the tools that will be compared

conda activate r_codes_mmbioit

#First, we will separate chromosomal sequences from plasmids using an in-house python script

echo "SEPARATING REFERENCE GENOMES INTO PLASMIDS AND CHROMOSOMES, THIS MIGHT TAKE A FEW HOURS"

python3 organize_reference_genomes.py

#----------------Now we will fix some of the problems that arise after separating the genomes-------------
cd ../data/separated_genomes/

cd GCA_000167875.2_ASM16787v2
sed -i -z 's/>CP035755.1/\n>CP035755.1/g' GCA_000167875.2_ASM16787v2_chromosome.fna

cd ../GCA_001874485.1_ASM187448v1
sed -i -z 's/>CP015087.1/\n>CP015087.1/g' GCA_001874485.1_ASM187448v1_chromosome.fna
sed -i -z 's/>CP015088.1/\n>CP015088.1/g' GCA_001874485.1_ASM187448v1_chromosome.fna

cd ../GCA_002925525.1_YDC107
sed -i -z 's/>CP025712.1/\n>CP025712.1/g' GCA_002925525.1_YDC107_chromosome.fna
sed -i -z 's/>CP025713.1/\n>CP025713.1/g' GCA_002925525.1_YDC107_chromosome.fna


#----------------------------------------------------------------------------------------------------------

#run an in-house bash script to get the strains identifiers
cd ../../../src
./get_identifiers.sh

#Make a list of strains that dont have plasmids

cd ../data/separated_genomes/
list_strains=$(ls)
for strains in $list_strains
do
cat ${strains}/${strains}_plasmids_count.csv >> ../general_plasmid_count.csv
done

cd ..

grep ',0' general_plasmid_count.csv > no_plasmid_strains.csv

sed -i 's/_genomic.fna//g' no_plasmid_strains.csv

cd ../src #I have to add it to the new code

#get data about the sra_accession codes to be able to retrieve the real short-reads after this.

conda activate ncbi_download_mmbioit

./get_sra_accessions.sh

conda activate r_codes_mmbioit

Rscript selecting_strains.R #this script will generate a list with all the strains that will be used for benchmarking, 270 strains, absent from All DB's.

cd ../results

sed -i 's/"//g' benchmark_list.csv

#---------------2. move FASTA files to a set location -----------------------------------------------

files=$(cat benchmark_list.csv | sed 's/"//g')

mkdir ../data/benchmark_genomes

cd ../data/benchmark_genomes

for strains in $files
do
cp ../../../2020_08_25_ecoli_metadata/data/fasta_final/${strains}* . #I might mv rather than cp
done

conda deactivate
#----------------------3.  Download the reads using sra-toolkit -------------------------------------------

conda activate sra_tools_mmbioit

cd ../../src

./get_sra_reads.sh #this script will create a directory for holding the reads: data/sra_files

#Now we will rename the reads appropiately  

./rename_sra_files.sh

conda deactivate

#-------------------4. Trim simulated reads using trim-galore-------------------------------------------------------------------

cd ../data

mkdir trimmed_reads

echo trimming_reads

conda activate trimming_mmbioit

cd sra_files/

#removed the --nextera argument from this command
trim_galore --quality 20 --dont_gzip --length 20 --paired -j 8 --output_dir ../trimmed_reads *fastq 

cd ../trimmed_reads

#rename the output files
for strains in $files
do
mv ${strains}_R1_val_1.fq ${strains}_R1.fq
mv ${strains}_R2_val_2.fq ${strains}_R2.fq
done


conda deactivate

#-----------------------5. Assembly genomes using spades-------------------------------------------------------------------------

echo 'running assembly with spades'

mkdir ../spades_assemblies

conda activate spades_mmbioit

for strains in $files
do
spades.py -1 ${strains}_R1.fq -2 ${strains}_R2.fq -o ../spades_assemblies/${strains} -k 37,57,77 --cov-cutoff 10.0 --threads 8 --careful
done

cd ..

##--------------------------5.1 Downsample 3 isoaltes and re-run SPAdes in isolate mode--------------------------#

#1. First we will move out the failed assemblies
mkdir failed_assemblies

mv spades_assmblies/GCA_014117345.1_ASM1411734v1 failed_assemblies/
mv spades_assmblies/GCA_006352265.1_ASM635226v1 failed_assemblies/
mv spades_assmblies/GCA_003812945.1_ASM381294v1 failed_assemblies/



#Now lets subsampled the reads
mkdir subsampled_reads

cd trimmed_reads

conda activate seqtk_mmbioit

seqtk sample -s100 GCA_003812945.1_ASM381294v1_R1.fq 2000000 > ../subsampled_reads/GCA_003812945.1_ASM381294v1_R1.fq
seqtk sample -s100 GCA_003812945.1_ASM381294v1_R2.fq 2000000 > ../subsampled_reads/GCA_003812945.1_ASM381294v1_R2.fq

seqtk sample -s100 GCA_006352265.1_ASM635226v1_R1.fq 2000000 > ../subsampled_reads/GCA_006352265.1_ASM635226v1_R1.fq
seqtk sample -s100 GCA_006352265.1_ASM635226v1_R2.fq 2000000 > ../subsampled_reads/GCA_006352265.1_ASM635226v1_R2.fq

seqtk sample -s100 GCA_014117345.1_ASM1411734v1_R1.fq 2000000 > ../subsampled_reads/GCA_014117345.1_ASM1411734v1_R1.fq
seqtk sample -s100 GCA_014117345.1_ASM1411734v1_R2.fq 2000000 > ../subsampled_reads/GCA_014117345.1_ASM1411734v1_R2.fq

cd ../subsampled_reads
failed_files=$(ls | cut -f 1,2,3 | sort -u)

for strains in $failed_files
do
spades.py -1 ${strains}_R1.fq -2 ${strains}_R2.fq -o ../spades_assemblies/${strains} -k 37,57,77 --cov-cutoff 10.0 --threads 8 --isolate
done

cd ..

#--------------------------6. Move files to specific folders, for easier access--------------------------#

#make directories to hold the results
mkdir contigs 
mkdir graphs 
mkdir fastg

cd spades_assemblies

#move contigs and graphs and re-name them
for strains in $files
do
mv ${strains}/scaffolds.fasta ../contigs/${strains}.fasta
mv ${strains}/assembly_graph_with_scaffolds.gfa ../graphs/${strains}.gfa
mv ${strains}/assembly_graph.fastg ../fastg/${strains}.fastg
done

#go back to the scripts directory

cd ../../src

#--------------------------7. Run gplas predictions----------------------------------------------------#

conda activate gplas_mmbioit
run_gplas_time_memory.sh
                

#--------------------------8. Run plasmidspades predictions -----------------------------------------#

conda activate spades_mmbioit

run_plasmidspades_time_memory.sh

#---------------------9. Run FishingForPlasmids-----------------------------------------------------#

conda activate fishing_plasmids_mmbioit

run_ffp_time_memory.sh

#--------------------10. Run HyAsP -------------------------------------------------------------------#

conda activate hyasp_mmbioit

run_hyasp_time_memory.sh

##--------------------11. Run MOB-suite--------------------------------------------------------------------##

conda activate mob-suite_mmbioit

run_mob_time_memory.sh

###-----------------12. Run SCAPP ----------------------------------------------------------------###

#activate environment
conda activate scapp_mmbioit

run_scapp_time_memory.sh

###------------13. Run an in-house python script for separating the hyasp predicitons into individual files-------###

conda activate r_codes_mmbioit

cd ../../2020_08_30_run_predictions/src

python separate_hyasp_bins.py

#rename the output so it includes the name of the strain
cd ../../tools/hyasp/predictions

#get a list for all the predictions
predictions=$(ls)

#loop to rename them, including the name of the strain infront of the name of the bin
for strains in $predictions
do
cd ${strains}
plasmids=$(ls bin*.fasta | sed 's/.fasta//g')
for bin in $plasmids
do
mv ${bin}.fasta ${strains}_${bin}.fasta
done
cd ..
done

###------------14. Run an in-house python script for separating the plasmid_spades and scapp predicitons into individual files-------###

cd ../../../2020_08_30_run_predictions/src

python separate_spades_bins.py

python separate_scapp_bins_fastg.py

###------- 15. Gather the time elapsed and memory used by each of the tools to make predictions
#get a list of the directories that hold the slurm scripts
directories=$(ls -d *_test_memory)

#loop thru directories
for directory in $directories
do
#get the name of the tool
tool=$(echo ${directory} | cut -f 1 -d _)
cd ${directory}
#get the list of sbatch jobs
jobs=$(ls slurm-* | cut -f 2 -d - | cut -f 1 -d .) 
for things in $jobs
do sacct --jobs=${things}.batch --user jpaganini --format="JobId,Elapsed,NCPUS,CPUTime,MaxRSS" | grep -v Job | grep -v '-' >> ../../results/${tool}_jobs_memory.txt
done
cd ..
done
