#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 20:30:56 2020

@author: juian
"""

import sys
import os
import glob

wd=os.path.dirname(os.path.realpath(__file__))
os.chdir(wd)
os.chdir('../../2020_08_25_ecoli_metadata/data/fasta_final')
genomes=glob.glob('*fna')


def organize_genome(input_file):
    strain=input_file.split('_')[0:3]
    strain='_'.join(strain)
    search='>'
    genes={}
    header=''
    sequence=[]
    seq=''
    
    #create dictionary with every sequence present in the fasta file (chr and plasmids)
    
    with open(input_file,'r') as f: 
        for line in f:
            if search in line:
                header=line
                genes[header]=''
                sequence=[]
                seq=''
            else:
                line=line.replace('\n','')
                sequence.append(line)
                seq=''.join(sequence)
                genes[header]=seq
    
    f.close()                
     
    #create folder for saving the results of each file
    general_dir='../../../2020_08_30_run_predictions/data/separated_genomes/'
    if not os.path.exists(general_dir):
        os.mkdir(general_dir)
    
    results_dir=general_dir+strain+'/'
    if not os.path.exists(results_dir):
        os.mkdir(results_dir)
    
    
    
    #separate chr and plasmids sequences into two different files          
    for header in genes.keys():
        if 'plasmid' in header:
            with open(results_dir+strain+'_plasmids.fna', 'a+') as p:
                p.write(header)
                p.write(genes[header])
                p.write('\n')
        else:
            with open(results_dir+strain+'_chromosome.fna', 'a+') as c:
                c.write(header)
                c.write(genes[header])
    
    c.close()
    try:
        p.close()
    except:
        pass
                
    with open(results_dir+strain+'_plasmids_count.csv','a+') as pc:
        plasmid_count=0
        for header in genes.keys():
            if 'plasmid' in header:
                plasmid_count+=1
        pc.write(input_file.split('/')[-1]+','+str(plasmid_count)+'\n')  
    pc.close()
    
    with open(results_dir+strain+'_plasmids_length.csv', 'a+') as l:
        for header,sequences in genes.items():
            if 'plasmid' in header:
                leng=len(sequences)
                header=header.replace('\n','')
                l.write(header+','+str(leng)+'\n') 
    l.close()    
        
    
for files in genomes:
    organize_genome(files)    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
