#!/bin/bash

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate ncbi_download_mmbioit

#1. Get a list of sra_ids 
#sra_accessions=$(cat ../results/benchmark_sra_list.csv) #use this line in the definitive script
sra_accessions=$(cat ../results/sra_rehab_missing.csv)

#2. Make a directory for holding the results
mkdir ../data/sra_files


#3. Make a loop to download the srr files
for reads in $sra_accessions
do
fasterq-dump --split-files ${reads} -O ../data/sra_files   
done
