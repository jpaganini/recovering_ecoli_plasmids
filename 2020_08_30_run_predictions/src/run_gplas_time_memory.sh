#!/bin/bash

mkdir gplas_slurm_jobs_test_memory

files=$(cat ../results/benchmark_list.csv)


#create slurm scripts

for strains in $files
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate gplas_mmbioit
cd ../../../tools/gplas
./gplas.sh -i ../../2020_08_30_run_predictions/data/graphs/${strains}.gfa -c mlplasmids -s 'Escherichia coli' -x 50 -n ${strains}" > gplas_slurm_jobs_test_memory/${strains}.sh
done

#run the scripts
cd gplas_slurm_jobs_test_memory
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=2:00:00 --mem=4G --job-name=gplas_test_memory --dependency=singleton ${slurm}
done
