#!/bin/bash

cd ../data/separated_genomes

files=$(ls)

for things in $files
do
if ls ${things}/${things}_chromosome.fna 1> /dev/null 2>&1
then
chromosomes=$(grep '>' ${things}/${things}_chromosome.fna | cut -f 1 -d ' ' | sed 's/>//g')
for sequences in $chromosomes
do echo ${things},${sequences} >> ../strains_chromosomes.csv
done
fi
if ls ${things}/${things}_plasmids.fna 1> /dev/null 2>&1
then
plasmids=$(grep '>' ${things}/${things}_plasmids.fna | cut -f 1 -d ' ' | sed 's/>//g')
for sequences in $plasmids
do echo ${things},${sequences} >> ../strains_plasmids.csv
done
fi
done
