#!/bin/bash

mkdir mob_slurm_jobs_test_memory
mkdir ../../tools/mob_predictions

files=$(cat ../results/benchmark_list.csv)


#create slurm scripts

for strains in $files
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate mob-suite_mmbioit
cd ../../../tools/mob_predictions
mob_recon --infile ../../2020_08_30_run_predictions/data/contigs/${strains}.fasta --outdir ${strains}" > mob_slurm_jobs_test_memory/${strains}.sh
done

#Run the scripts
cd mob_slurm_jobs_test_memory
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=2:00:00 --mem=4G ${slurm} --job-name=mob_test_memory
done


