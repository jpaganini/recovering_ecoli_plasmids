#!/bin/bash

mkdir ffp_slurm_jobs_test_memory
files=$(cat ../results/benchmark_list.csv)

#create slurm scripts

for strains in $files
do
echo "#!/bin/bash
#activate environment
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate fishing_plasmids_mmbioit

#move to the directory that contains the assemblies
cd ../../data/contigs
#remove previous assemblies (because we want to do it one by one)
rm ../../../tools/FishingForPlasmids/data/assemblyDir/*
#copy the apropiate strain to the assemblies directory
cp ${strains}.fasta ../../../tools/FishingForPlasmids/data/assemblyDir/
#move to the ffp directory
cd ../../../tools/FishingForPlasmids/
#run the configuration file
python3 scripts/make_config_file.py
#run the tool
python3 FFP.py" > ffp_slurm_jobs_test_memory/${strains}.sh
done

#run the scripts
cd ffp_slurm_jobs_test_memory
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=2:00:00 --mem=4G --job-name=ffp_test_memory --dependency=singleton ${slurm}
done
