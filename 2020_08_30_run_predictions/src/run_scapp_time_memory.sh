#!/bin/bash

#create directory for the sbatch jobs
mkdir scapp_slurm_jobs_test_memory
#create directory for holdin scapp predictions
mkdir ../../tools/scapp_predictions

#1. Create a list of files
files=$(cat ../results/benchmark_list.csv)


#create slurm scripts

for strains in $files
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate scapp_mmbioit
cd ../../../tools/scapp_predictions
scapp -g ../../2020_08_30_run_predictions/data/fastg/${strains}.fastg -o ${strains} -r1 ../../2020_08_30_run_predictions/data/trimmed_reads/${strains}_R1.fq -r2 ../../2020_08_30_run_predictions/data/trimmed_reads/${strains}_R2.fq" > scapp_slurm_jobs_test_memory/${strains}.sh
done

#run the scripts
cd scapp_slurm_jobs_test_memory
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=4:00:00 --mem=4G ${slurm}
done

