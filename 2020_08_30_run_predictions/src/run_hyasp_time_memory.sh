#!/bin/bash
#make directory for sbatch scripts
mkdir hyasp_slurm_jobs_test_memory
#make directory for the mapping output of hyasp
mkdir ../../tools/hyasp/map_output

files=$(cat ../results/benchmark_list.csv)

for strains in $files
do
echo "#!/bin/bash
source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate hyasp_mmbioit
#change directory
cd ../../../tools/hyasp
#change directory for mapping
cd map_output
#1. mapping graphs against reference genes
hyasp map ../databases/ncbi_database_genes.fasta ${strains}_gcm.csv -c -g ../../../2020_08_30_run_predictions/data/graphs/${strains}.gfa
hyasp filter ../databases/ncbi_database_genes.fasta ${strains}_gcm.csv filtered_${strains}_gcm.csv
#move to the prediction folder
cd ../predictions
#find the predictions
hyasp find -k 0.6 -b 2.5 -g 0.5 -l 1000 -L 700000 ../../../2020_08_30_run_predictions/data/graphs/${strains}.gfa ../databases/ncbi_database_genes.fasta ../map_output/filtered_${strains}_gcm.csv ${strains}" > hyasp_slurm_jobs_test_memory/${strains}.sh
done

#run the scripts
cd hyasp_slurm_jobs_test_memory
jobs=$(ls)
for slurm in $jobs
do
sbatch --time=2:00:00 --mem=4G --job-name=hyasp_test_memory --dependency=singleton ${slurm}
done
